# Репозиторий группы вариантов "warehouse-app"
Этот репозиторий был создан для выполнения лабораторного практикума по курсу
"Архитектура информационных систем" осеннего семестра 2020 года.

# Описание заданий
Задание по варианту состоит из `предметной области` и `области автоматизации`.

`Предметная область` - это направление деятельности некоторой вымышленной
организации. Деятельность организации включает в себя множество
бизнес-процессов для достижения бизнес-целей.

`Область автоматизации` - это подмножество бизнес-процессов, для автоматизации
которых создается информационная система.

Для выполнения практикума необходимо создать информационную систему, позволяющую
автоматизировать бизнес-процессы области автоматизации.

Этот репозиторий объединяет несколько вариантов, связанный с одной предметной
областью. Набор областей автоматизации и распределение бизнесс-процессов между
ними станет известен после завершения аналитики. До этого момента студенты,
назначенные на этот репозиторий работают вместе.

## Предметная область
Предметная область данного задания - `мобильное приложение и облачный сервис для
управления перемещением продукции на складах`
### Мобильное приложение и облачный сервис для управления перемещением продукции на складах
Приложение для андроид-устройств (терминалов сбора данных), позволяющее
выполнять учётные операции на складах продукции. Приложение, также, должно
позволять осуществлять перемещение продукции внутри и между складами.

## Область автоматизации
Области автоматизации предметной области предстоит выяснить на этапе аналитики.

# Преподаватель
Каждый вариант закреплен за своим преподавателем.
Преподаватель для вариантов этого репозитория: **Прокофьев Антон Олегович** (см.
контакты ниже).

# Организация репозиториев
Все репозитории являются открытыми. Это означает, что любой может просмотреть
хранящийся в них код или склонировать его.
Студентам, назначенным на репозиторий, предоставляются права главного
разработчика (maintainer). Он может совершать коммиты в репозиторий, создавать
ветви и т.д.

## Рабочее дерево репозитория
Рабочее дерево репозитория имеет следующую структуру:
```
.
├── docs
├── areas
└── README.md
```
Директория `docs` предназначена для хранения результатов выполнения заданий по
аналитике. В директории `areas` будет располагаться реализация
(исходный код, тесты и т.д.) и документация информационных систем областей
автоматизации.

## Карточки заданий
Обратите внимание на страницу 'Issues'. На ней представлены карточки,
соответствующие заданиям по варианту (на всех таких карточках имеется синяя
метка 'course'). Каждая карточка имеет установленную дату выполнения и связана
с одним из разделов практикума. С их помощью вы можете получить информацию о
выполненных и невыполненных заданиях, а также о сроках выполнения заданий и о
сроках контроля разделов. Карточки, на которые вы назначены исполнителем,
представляют задания, которые вам требуется выполнить по этому варианту.

> **ВНИМАНИЕ**: Карточки, отмеченные синей меткой 'course' предназначены для
обратной связи от преподавателей о прогрессе студента в выполнении задания.
Студенту нельзя вносить в них изменения (однако, можно комментировать, связывать
с коммитами, указывать оценку трудозатрат и вести учёт затраченного времени).

> В этом репозитории изначально созданы только карточки аналитических заданий.
Позже, после определения областей автоматизации, будут добавлены карточки
остальных заданий.

При необходимости, студент может создавать собственные карточки для личного
пользования.

Напоминаем, что все результаты этапов практикума (будь то код или прочие
артефакты) должны быть загружены в репозиторий. Студентам, не имеющим опыта
работы с Git, рекоментуется для ознакомления книга "Pro Git" за авторством Scott
Chacon и Ben Straub (см. ссылки ниже). Рекомендуется связывать коммиты,
которыми загружаются результаты этапа, с карточкой соответствующего задания.
Подробнее о том как это делать см. [по этой ссылке](https://docs.gitlab.com/ee/user/project/issues/crosslinking_issues.html#from-commit-messages).

# Полезные ссылки
Ниже перечислены некоторые полезные ссылки.

  - [Группа "АИС-осень-2020"](https://gitlab.com/mephi.isa/2020-autumn)
  - [Контрольные вехи](https://gitlab.com/groups/mephi.isa/2020-autumn/-/milestones)
  - [Задачи по этому варианту](https://gitlab.com/mephi.isa/2020-autumn/warehouse-app/-/issues)
  - [Pro Git by Scott Chacon and Ben Straub](https://git-scm.com/book/en/v2)

# Контакты
Ниже перечислены контакты преподавателей:

1. Прокофьев Антон Олегович
   - email: kriomer@mail.ru
   - gitlab: @kriomer
   - telegram: kriomer
1. Степанов Максим Максимович
   - email: mmstepanov@mephi.ru
   - gitlab: @delatrie
   - telegram: delatrie
1. Шустова Лариса Ивановна
   - email: shust-li@yandex.ru
   - gitlab: @Shust-li